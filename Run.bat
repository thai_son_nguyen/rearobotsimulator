SET MSBuildPath = "C:\Windows\Microsoft.NET\Framework64\v4.0.30319\"
SET PATH=%PATH%;MSBuildPath

@echo off
cls

msbuild.exe "REARobotSimulator.sln" /p:configuration=release /p:OutputPath="..\BuildResult" /t:Rebuild
cls
BuildResult\REARobotSimulator.exe