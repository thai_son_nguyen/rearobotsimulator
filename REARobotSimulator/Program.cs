﻿using REARobotSimulator.Command;
using REARobotSimulator.Controller;
using REARobotSimulator.Navigation;
using REARobotSimulator.Robot;

namespace REARobotSimulator
{
    class Program
    {
        static void Main(string[] args)
        {
            var sensor = new SimpleSurfaceSensor(5, 5);
            var robot = new ToyRobot(sensor);
            var commandInvokder = new SynCommandInvoker();
            var controller = new ConsoleController(robot, commandInvokder);
            controller.Start();
        }
    }
}
