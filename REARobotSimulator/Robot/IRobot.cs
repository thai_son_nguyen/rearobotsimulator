﻿using REARobotSimulator.Navigation;

namespace REARobotSimulator.Robot
{
    public interface IRobot
    {
        /// <summary>
        /// Get current location
        /// </summary>
        /// <returns></returns>
        Coordinate CurrentLocation { get; }

        /// <summary>
        /// Get current direction
        /// </summary>
        Direction CurrentDirection { get; }

        /// <summary>
        /// Check whether robot is ready to perform action
        /// </summary>
        bool IsReady { get; }

        /// <summary>
        /// Place robot given location and direction
        /// </summary>
        /// <param name="location"></param>
        /// <param name="direction"></param>
        void Place(Coordinate location, Direction direction);

        /// <summary>
        /// Move forward
        /// </summary>
        void Move();

        /// <summary>
        /// Turn left
        /// </summary>
        void TurnLeft();

        /// <summary>
        /// Turn right
        /// </summary>
        void TurnRight();

        /// <summary>
        /// Reset all data and state
        /// </summary>
        void Reset();
    }
}
