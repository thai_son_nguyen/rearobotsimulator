﻿using REARobotSimulator.Navigation;

namespace REARobotSimulator.Robot
{
    /// <summary>
    /// Robot class
    /// </summary>
    public class ToyRobot : IRobot
    {
        private readonly ISurfaceSensor _sensor;
        public Coordinate CurrentLocation { get; private set; }
        public Direction CurrentDirection { get; private set; }
        public bool IsReady { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="sensor"></param>
        public ToyRobot(ISurfaceSensor sensor)
        {
            _sensor = sensor;
            Reset();
        }

        #region Implementation of IRobot

        /// <summary>
        /// Place robot given location and direction
        /// </summary>
        /// <param name="location"></param>
        /// <param name="direction"></param>
        public void Place(Coordinate location, Direction direction)
        {
            if (_sensor.IsOccupiable(location))
            {
                CurrentDirection = direction;
                CurrentLocation = location;
                IsReady = true;
            }
        }

        /// <summary>
        /// Move forward
        /// </summary>
        public void Move()
        {
            if (!IsReady) return;
            Coordinate newLocation = null;
            switch (CurrentDirection)
            {
                case Direction.EAST:
                    newLocation = new Coordinate(CurrentLocation.X + 1, CurrentLocation.Y);
                    break;
                case Direction.NORTH:
                    newLocation = new Coordinate(CurrentLocation.X, CurrentLocation.Y + 1);
                    break;
                case Direction.SOUTH:
                    newLocation = new Coordinate(CurrentLocation.X, CurrentLocation.Y - 1);
                    break;
                case Direction.WEST:
                    newLocation = new Coordinate(CurrentLocation.X - 1, CurrentLocation.Y);
                    break;
            }
            if (_sensor.IsOccupiable(newLocation))
            {
                CurrentLocation = newLocation;
            }
        }

        /// <summary>
        /// Turn left
        /// </summary>
        public void TurnLeft()
        {
            if (!IsReady) return;

            switch (CurrentDirection)
            {
                case Direction.NORTH:
                    CurrentDirection = Direction.WEST;
                    break;
                case Direction.SOUTH:
                    CurrentDirection = Direction.EAST;
                    break;
                case Direction.EAST:
                    CurrentDirection = Direction.NORTH;
                    break;
                case Direction.WEST:
                    CurrentDirection = Direction.SOUTH;
                    break;
                default:
                    CurrentDirection = CurrentDirection;
                    break;
            }
        }

        /// <summary>
        /// Turn right
        /// </summary>
        public void TurnRight()
        {
            if (!IsReady) return;

            switch (CurrentDirection)
            {
                case Direction.NORTH:
                    CurrentDirection = Direction.EAST;
                    break;
                case Direction.SOUTH:
                    CurrentDirection = Direction.WEST;
                    break;
                case Direction.EAST:
                    CurrentDirection = Direction.SOUTH;
                    break;
                case Direction.WEST:
                    CurrentDirection = Direction.NORTH;
                    break;
                default:
                    CurrentDirection = CurrentDirection;
                    break;
            }
        }

        /// <summary>
        /// Reset all data and state
        /// </summary>
        public void Reset()
        {
            CurrentDirection = Direction.NORTH;
            CurrentLocation = null;
            IsReady = false;
        }

        #endregion
    }
}
