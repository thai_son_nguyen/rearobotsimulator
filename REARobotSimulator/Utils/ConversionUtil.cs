﻿using REARobotSimulator.Navigation;

namespace REARobotSimulator.Utils
{
    public static class ConversionUtil
    {
        /// <summary>
        /// Convert string to nullable int
        /// </summary>
        /// <param name="strValue"></param>
        /// <returns></returns>
        public static int? ToNullableInt(string strValue)
        {            
            if (!string.IsNullOrEmpty(strValue))
            {
                int intValue;
                if (int.TryParse(strValue, out intValue))
                {
                    return intValue;
                }
            }
            return null;
        }

        /// <summary>
        /// Convert string to Direction enum
        /// </summary>
        /// <param name="strValue"></param>
        /// <returns></returns>
        public static Direction? ToDirectionEnum(string strValue)
        {
            Direction direction;
            if (System.Enum.TryParse(strValue, true, out direction))
            {
                return direction;
            }
            return null;
        }
    }
}
