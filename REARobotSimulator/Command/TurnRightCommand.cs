﻿using REARobotSimulator.Robot;

namespace REARobotSimulator.Command
{
    /// <summary>
    /// Turn right command
    /// </summary>
    public class TurnRightCommand : CommandBase
    {
        public TurnRightCommand(IRobot receiver) : base(receiver)
        {
        }

        #region Overrides of CommandBase

        public override void Execute()
        {
            Receiver.TurnRight();
        }

        #endregion
    }
}
