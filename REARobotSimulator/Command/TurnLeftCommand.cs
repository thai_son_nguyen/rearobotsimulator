﻿using REARobotSimulator.Robot;

namespace REARobotSimulator.Command
{
    /// <summary>
    /// Turn left command
    /// </summary>
    public class TurnLeftCommand : CommandBase
    {
        public TurnLeftCommand(IRobot receiver) : base(receiver)
        {
        }

        #region Overrides of CommandBase

        public override void Execute()
        {
            Receiver.TurnLeft();
        }

        #endregion
    }
}
