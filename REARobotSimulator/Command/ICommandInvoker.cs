﻿namespace REARobotSimulator.Command
{
    public interface ICommandInvoker
    {
        /// <summary>
        /// Invoke command
        /// </summary>
        /// <param name="command"></param>
        void Invoke(ICommand command);
    }
}
