﻿using REARobotSimulator.Robot;

namespace REARobotSimulator.Command
{
    /// <summary>
    /// Null command which doesn't do anything
    /// </summary>
    public class NullCommand : CommandBase
    {
        public NullCommand(IRobot receiver) : base(receiver)
        {
        }

        #region Overrides of CommandBase

        /// <summary>
        /// Does not do anything
        /// </summary>
        public override void Execute()
        {
        }

        #endregion
    }
}
