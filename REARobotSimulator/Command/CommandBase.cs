﻿using REARobotSimulator.Robot;

namespace REARobotSimulator.Command
{
    /// <summary>
    /// Base command class
    /// </summary>
    public abstract class CommandBase : ICommand
    {
        protected IRobot Receiver { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="receiver"></param>
        protected CommandBase(IRobot receiver)
        {
            Receiver = receiver;
        }        
        
        #region Implementation of IRobotCommand

        public abstract void Execute();

        #endregion
    }
}
