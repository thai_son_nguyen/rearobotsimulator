﻿namespace REARobotSimulator.Command
{
    public interface ICommand
    {
        void Execute();
    }
}
