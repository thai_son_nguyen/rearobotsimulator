﻿using System;
using REARobotSimulator.Navigation;
using REARobotSimulator.Robot;

namespace REARobotSimulator.Command
{
    public class PlaceCommand : CommandBase
    {
        private readonly Direction _direction;
        private readonly Coordinate _location;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="location"></param>
        /// <param name="direction"></param>
        /// <param name="receiver"></param>
        public PlaceCommand(Coordinate location, Direction direction, IRobot receiver) : base(receiver)
        {
            _location = location;
            _direction = direction;
        }

        #region Overrides of RobotCommandBase

        /// <summary>
        /// Execute command
        /// </summary>
        public override void Execute()
        {
            if (Receiver != null)
            {
                Receiver.Place(_location, _direction);
            }
        }

        #endregion
    }
}
