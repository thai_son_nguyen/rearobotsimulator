﻿using System;
using REARobotSimulator.Robot;

namespace REARobotSimulator.Command
{
    /// <summary>
    /// Report command
    /// </summary>
    public class ReportCommand : CommandBase
    {
        public ReportCommand(IRobot receiver) : base(receiver)
        {
        }

        #region Overrides of CommandBase

        public override void Execute()
        {
            if (Receiver.IsReady && Receiver.CurrentLocation != null)
            {
                Console.WriteLine(string.Format("{0},{1},{2}", Receiver.CurrentLocation.X, Receiver.CurrentLocation.Y,
                    Receiver.CurrentDirection));
            }
        }

        #endregion
    }
}
