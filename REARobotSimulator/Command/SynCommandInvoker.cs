﻿namespace REARobotSimulator.Command
{
    /// <summary>
    /// Synchronous command invoker
    /// </summary>
    public class SynCommandInvoker : ICommandInvoker
    {
        #region Implementation of ICommandInvoker

        public void Invoke(ICommand command)
        {
            if (command != null)
            {
                command.Execute();
            }
        }

        #endregion
    }
}
