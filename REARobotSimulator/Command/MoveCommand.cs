﻿using System;
using REARobotSimulator.Robot;

namespace REARobotSimulator.Command
{
    /// <summary>
    /// Move command
    /// </summary>
    public class MoveCommand : CommandBase
    {
        public MoveCommand(IRobot receiver) : base(receiver)
        {
        }

        #region Overrides of CommandBase

        public override void Execute()
        {
            Receiver.Move();
        }

        #endregion
    }
}
