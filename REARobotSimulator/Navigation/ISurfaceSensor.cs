﻿namespace REARobotSimulator.Navigation
{
    /// <summary>
    /// Behaviour constract for surface sensor
    /// </summary>
    public interface ISurfaceSensor
    {
        /// <summary>
        /// Check whether a location can be occupied
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        bool IsOccupiable(Coordinate location);
    }
}
