﻿using System;

namespace REARobotSimulator.Navigation
{
    /// <summary>
    /// Simple sensor for fix flat surface
    /// </summary>
    public class SimpleSurfaceSensor : ISurfaceSensor
    {
        public Coordinate TopLeft { get; private set; }
        public Coordinate BottomRight { get; private set; }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public SimpleSurfaceSensor(int width, int height)
        {
            if (width <= 0)
            {
                throw new ArgumentException(
                    "Width must be greater than 0 !");
            }
            if (height <= 0)
            {
                throw new ArgumentException(
                    "Height must be greater than 0 !");
            }
            TopLeft = new Coordinate(0, height - 1);
            BottomRight = new Coordinate(width - 1, 0);
        }

        #region Implementation of ISurfaceSensor

        public bool IsOccupiable(Coordinate location)
        {
            return location != null && TopLeft.X <= location.X && location.X <= BottomRight.X &&
                   BottomRight.Y <= location.Y && location.Y <= TopLeft.Y;
        }

        #endregion
    }
}
