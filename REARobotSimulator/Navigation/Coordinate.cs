﻿namespace REARobotSimulator.Navigation
{
    /// <summary>
    /// Coordinate
    /// </summary>
    public class Coordinate
    {        
        public int X { get; private set; }
        public int Y { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Coordinate(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}
