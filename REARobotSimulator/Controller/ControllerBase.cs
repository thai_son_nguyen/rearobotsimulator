﻿using System;
using REARobotSimulator.Command;
using REARobotSimulator.Robot;

namespace REARobotSimulator.Controller
{
    public abstract class ControllerBase : IController
    {
        protected IRobot TargetRobot { get; private set; }

        protected ICommandInvoker CommandInvoker { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="targetRobot"></param>
        /// <param name="commandInvoker"></param>
        protected ControllerBase(IRobot targetRobot, ICommandInvoker commandInvoker)
        {
            if (targetRobot == null)
            {
                throw new ArgumentException("Need robot to control !");
            }
            if (commandInvoker == null)
            {
                throw new ArgumentException("Need command invoker !");
            }
            TargetRobot = targetRobot;
            CommandInvoker = commandInvoker;
        }

        #region Implementation of IController

        public abstract void Start();

        #endregion

        #region Protected methods
        /// <summary>
        /// Invoke command
        /// </summary>
        /// <param name="command"></param>
        protected void InvokeCommand(ICommand command)
        {
            CommandInvoker.Invoke(command);
        }
        #endregion
    }
}
