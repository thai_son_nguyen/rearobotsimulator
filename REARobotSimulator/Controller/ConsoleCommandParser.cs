﻿using System.Text.RegularExpressions;
using REARobotSimulator.Command;
using REARobotSimulator.Navigation;
using REARobotSimulator.Robot;
using REARobotSimulator.Utils;

namespace REARobotSimulator.Controller
{
    /// <summary>
    /// Console command parser
    /// </summary>
    public static class ConsoleCommandParser
    {
        private static readonly Regex PlaceCommandPattern = new Regex("^PLACE\\s+(\\d+),(\\d+),([a-zA-Z]+)$",
            RegexOptions.IgnoreCase);

        private static readonly Regex MoveCommandPattern = new Regex("^MOVE$", RegexOptions.IgnoreCase);

        private static readonly Regex LeftCommandPattern = new Regex("^LEFT$", RegexOptions.IgnoreCase);

        private static readonly Regex RightCommandPattern = new Regex("^RIGHT$", RegexOptions.IgnoreCase);

        private static readonly Regex ReportCommandPattern = new Regex("^REPORT$", RegexOptions.IgnoreCase);

        /// <summary>
        /// Parse command
        /// </summary>
        /// <param name="intputCommand"></param>
        /// <param name="targetRobot"></param>
        /// <returns></returns>
        public static ICommand ParseCommand(string intputCommand, IRobot targetRobot)
        {
            var commandStr = intputCommand.Trim();

            ICommand command;
            if (MoveCommandPattern.IsMatch(commandStr))
            {
                command = new MoveCommand(targetRobot);
            }
            else if (LeftCommandPattern.IsMatch(commandStr))
            {
                command = new TurnLeftCommand(targetRobot);
            }
            else if (RightCommandPattern.IsMatch(commandStr))
            {
                command = new TurnRightCommand(targetRobot);
            }
            else if (ReportCommandPattern.IsMatch(commandStr))
            {
                command = new ReportCommand(targetRobot);
            }
            else if (PlaceCommandPattern.IsMatch(commandStr))
            {
                command = CreatePlaceCommand(commandStr, targetRobot);
            }
            else
            {
                command = new NullCommand(targetRobot);
            }
            return command;
        }

        /// <summary>
        /// Create place command
        /// </summary>
        /// <param name="commandStr"></param>
        /// <param name="targetRobot"></param>
        /// <returns></returns>
        private static ICommand CreatePlaceCommand(string commandStr, IRobot targetRobot)
        {
            var matches = PlaceCommandPattern.Matches(commandStr);
            if (matches.Count == 1 && matches[0].Groups.Count == 4)
            {
                var matchGroups = matches[0].Groups;
                var xCoordinate = ConversionUtil.ToNullableInt(matchGroups[1].ToString());
                var yCoordinate = ConversionUtil.ToNullableInt(matchGroups[2].ToString());
                var direction = ConversionUtil.ToDirectionEnum(matchGroups[3].ToString());
                if (xCoordinate.HasValue && yCoordinate.HasValue && direction.HasValue)
                {
                    return new PlaceCommand(new Coordinate(xCoordinate.Value, yCoordinate.Value), direction.Value,
                        targetRobot);
                }
            }
            return new NullCommand(targetRobot);
        }
    }
}
