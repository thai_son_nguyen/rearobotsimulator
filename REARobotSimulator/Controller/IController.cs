﻿namespace REARobotSimulator.Controller
{
    public interface IController
    {
        /// <summary>
        /// Start controller
        /// </summary>
        void Start();
    }
}
