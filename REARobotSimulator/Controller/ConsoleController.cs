﻿using System;
using System.Text.RegularExpressions;
using REARobotSimulator.Command;
using REARobotSimulator.Robot;

namespace REARobotSimulator.Controller
{
    public class ConsoleController : ControllerBase
    {
        #region Properties

        private static readonly Regex ExitCommandPattern = new Regex("^EXIT$", RegexOptions.IgnoreCase);

        #endregion

        #region Contructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="targetRobot"></param>
        /// <param name="commandInvoker"></param>
        public ConsoleController(IRobot targetRobot, ICommandInvoker commandInvoker) : base(targetRobot, commandInvoker)
        {
        }

        #endregion

        #region Overrides of ControllerBase

        /// <summary>
        /// 
        /// </summary>
        public override void Start()
        {
            DisplayWelcome();
            /* Read user input */
            var isExist = false;
            while (!isExist)
            {
                // Read command
                var inputCommand = GetInput();
                // Exit command
                if (ExitCommandPattern.IsMatch(inputCommand))
                {
                    isExist = true;
                    Output("Bye !");
                }
                /* Other command */
                InvokeCommand(ConsoleCommandParser.ParseCommand(inputCommand, TargetRobot));
            }
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Display welcome message
        /// </summary>
        private static void DisplayWelcome()
        {
            Output("Welcome to robot simulator !");
            Output("1. PLACE X,Y,F -> To place robot and start (with F = NORTH|SOUTH|WEST|EAST).");
            Output("2. MOVE -> To move robot forward.");
            Output("3. LEFT -> To turn left.");
            Output("4. RIGHT -> To turn right.");
            Output("5. REPORT -> To report location.");
            Output("6. EXIT -> To quit program.");
        }

        /// <summary>
        /// Read user input
        /// </summary>
        /// <returns></returns>
        private static string GetInput()
        {
            Console.Write(">");
            return Console.ReadLine();
        }

        /// <summary>
        /// Output a string to console
        /// </summary>
        /// <param name="outputStr"></param>
        private static void Output(string outputStr)
        {
            Console.WriteLine(outputStr);
        }

        #endregion
    }
}
