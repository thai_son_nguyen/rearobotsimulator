﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using REARobotSimulator.Navigation;

namespace REARobotSimulatorTest.Navigation
{
    [TestClass]
    public class SimpleSurfaceSensorTest
    {
        /// <summary>
        /// Test invalid width
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void WhenSurfaceWidthIsInValidThenThereWillBeArgumentException()
        {
            var sensor = new SimpleSurfaceSensor(-1, 1);
            Assert.IsNull(sensor);
        }

        /// <summary>
        /// Test invalid height
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void WhenSurfaceHeightIsInValidThenThereWillBeArgumentException()
        {
            var sensor = new SimpleSurfaceSensor(1, -1);
            Assert.IsNull(sensor);
        }

        /// <summary>
        /// Test valid sensor
        /// </summary>
        [TestMethod]
        public void WhenSurfaceWidthAndHeightAreValidThenSensorWillBeCreatedSuccessfully()
        {
            const int width = 5;
            const int height = 10;
            var sensor = new SimpleSurfaceSensor(width, height);

            /* Check result */
            var topLeft = sensor.TopLeft;
            var bottomRight = sensor.BottomRight;

            Assert.AreEqual(0, topLeft.X);
            Assert.AreEqual(height - 1, topLeft.Y);
            Assert.AreEqual(width - 1, bottomRight.X);
            Assert.AreEqual(0, bottomRight.Y);
        }

        /// <summary>
        /// Test occupiable
        /// </summary>
        [TestMethod]
        public void WhenACoordinateHasBothXAndYWithinSurfaceBoundaryThenItCanBeOccupied()
        {
            /* Set up data */
            var sensor = new SimpleSurfaceSensor(5, 5);
            var location = new Coordinate(1,1);
            /* Test */
            var isOccupiable = sensor.IsOccupiable(location);
            /* Check result */
            Assert.IsTrue(isOccupiable);
        }

        /// <summary>
        /// Test occupiable
        /// </summary>
        [TestMethod]
        public void WhenACoordinateHasXSurfaceBoundaryButNotYThenItCanNotBeOccupied()
        {
            /* Set up data */
            var sensor = new SimpleSurfaceSensor(5, 5);
            var location = new Coordinate(1, 6);
            /* Test */
            var isOccupiable = sensor.IsOccupiable(location);
            /* Check result */
            Assert.IsFalse(isOccupiable);
        }

        /// <summary>
        /// Test occupiable
        /// </summary>
        [TestMethod]
        public void WhenACoordinateHasYSurfaceBoundaryButNotXThenItCanNotBeOccupied()
        {
            /* Set up data */
            var sensor = new SimpleSurfaceSensor(5, 5);
            var location = new Coordinate(6, 1);
            /* Test */
            var isOccupiable = sensor.IsOccupiable(location);
            /* Check result */
            Assert.IsFalse(isOccupiable);
        }
    }
}
