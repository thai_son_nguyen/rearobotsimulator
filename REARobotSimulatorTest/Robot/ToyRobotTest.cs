﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using REARobotSimulator.Command;
using REARobotSimulator.Navigation;
using REARobotSimulator.Robot;

namespace REARobotSimulatorTest.Robot
{
    [TestClass]
    public class ToyRobotTest
    {
        #region Test initial state

        /// <summary>
        /// Test construction
        /// </summary>
        [TestMethod]
        public void WhenRobotHasNotBeenPlacedThenItIsNotReady()
        {
            /* Set up data */
            var sensor = new Mock<ISurfaceSensor>();
            /* Test */
            var robot = new ToyRobot(sensor.Object);
            /* Check result */
            Assert.IsFalse(robot.IsReady);
            Assert.IsNull(robot.CurrentLocation);
        }

        #endregion

        #region Test place

        /// <summary>
        /// Test place
        /// </summary>
        [TestMethod]
        public void WhenRobotHasBeenPlacedThenItIsReady()
        {
            /* Set up data */
            var sensor = new Mock<ISurfaceSensor>();
            var coordinate = new Coordinate(1, 1);
            var direction = Direction.SOUTH;

            sensor.Setup(s => s.IsOccupiable(coordinate)).Returns(true);

            /* Test */
            var robot = new ToyRobot(sensor.Object);
            robot.Place(coordinate, direction);

            /* Check result */
            Assert.IsTrue(robot.IsReady);
            Assert.AreEqual(coordinate.X, robot.CurrentLocation.X);
            Assert.AreEqual(coordinate.Y, robot.CurrentLocation.Y);
            Assert.AreEqual(direction, robot.CurrentDirection);
        }

        /// <summary>
        /// Test invalid place
        /// </summary>
        [TestMethod]
        public void WhenFirstPlaceIsDoneWithIncorrectCoordinateThenCommandWillBeIgnoredAndRobotIsNotReady()
        {
            /* Set up data */
            var sensor = new Mock<ISurfaceSensor>();
            var coordinate = new Coordinate(1, 1);
            var direction = Direction.SOUTH;
            sensor.Setup(s => s.IsOccupiable(coordinate)).Returns(false);

            /* Test */
            var robot = new ToyRobot(sensor.Object);
            robot.Place(coordinate, direction);

            /* Check result */
            Assert.IsFalse(robot.IsReady);
            Assert.IsNull(robot.CurrentLocation);
        }

        /// <summary>
        /// Test invalid place
        /// </summary>
        [TestMethod]
        public void WhenPerformAnyPlaceWithIncorrectCoordinateThenCommandWillBeIgnored()
        {
            /* Set up data */
            var sensor = new Mock<ISurfaceSensor>();
            var coordinate = new Coordinate(1, 1);
            var direction = Direction.SOUTH;
            sensor.Setup(s => s.IsOccupiable(coordinate)).Returns(true);

            var invalidCoordinate = new Coordinate(1, 1);
            var invalidDirection = Direction.WEST;
            sensor.Setup(s => s.IsOccupiable(invalidCoordinate)).Returns(false);

            var robot = new ToyRobot(sensor.Object);
            robot.Place(coordinate, direction);

            /* Test */
            robot.Place(invalidCoordinate, invalidDirection);

            /* Check result */
            Assert.IsTrue(robot.IsReady);
            Assert.AreEqual(coordinate.X, robot.CurrentLocation.X);
            Assert.AreEqual(coordinate.Y, robot.CurrentLocation.Y);
            Assert.AreEqual(direction, robot.CurrentDirection);
        }

        #endregion

        #region Test move

        /// <summary>
        /// Test move NORTH
        /// </summary>
        [TestMethod]
        public void WhenRobotIsFacingNorthThenMoveWillMakeOneStepForwardNorth()
        {
            /* Set up data */
            var sensor = new Mock<ISurfaceSensor>();
            var coordinate = new Coordinate(1, 1);
            const Direction direction = Direction.NORTH;

            sensor.Setup(s => s.IsOccupiable(It.IsAny<Coordinate>())).Returns(true);

            var robot = new ToyRobot(sensor.Object);
            robot.Place(coordinate, direction);

            /* Test */
            robot.Move();

            /* Check result */
            Assert.AreEqual(coordinate.X, robot.CurrentLocation.X);
            Assert.AreEqual(coordinate.Y + 1, robot.CurrentLocation.Y);
            Assert.AreEqual(direction, robot.CurrentDirection);
        }

        /// <summary>
        /// Test move SOUTH
        /// </summary>
        [TestMethod]
        public void WhenRobotIsFacingSouthThenMoveWillMakeOneStepForwardSouth()
        {
            /* Set up data */
            var sensor = new Mock<ISurfaceSensor>();
            var coordinate = new Coordinate(1, 1);
            const Direction direction = Direction.SOUTH;

            sensor.Setup(s => s.IsOccupiable(It.IsAny<Coordinate>())).Returns(true);

            var robot = new ToyRobot(sensor.Object);
            robot.Place(coordinate, direction);

            /* Test */
            robot.Move();

            /* Check result */
            Assert.AreEqual(coordinate.X, robot.CurrentLocation.X);
            Assert.AreEqual(coordinate.Y - 1, robot.CurrentLocation.Y);
            Assert.AreEqual(direction, robot.CurrentDirection);
        }

        /// <summary>
        /// Test move WEST
        /// </summary>
        [TestMethod]
        public void WhenRobotIsFacingWestThenMoveWillMakeOneStepForwardWest()
        {
            /* Set up data */
            var sensor = new Mock<ISurfaceSensor>();
            var coordinate = new Coordinate(1, 1);
            const Direction direction = Direction.WEST;

            sensor.Setup(s => s.IsOccupiable(It.IsAny<Coordinate>())).Returns(true);

            var robot = new ToyRobot(sensor.Object);
            robot.Place(coordinate, direction);

            /* Test */
            robot.Move();

            /* Check result */
            Assert.AreEqual(coordinate.X - 1, robot.CurrentLocation.X);
            Assert.AreEqual(coordinate.Y, robot.CurrentLocation.Y);
            Assert.AreEqual(direction, robot.CurrentDirection);
        }

        /// <summary>
        /// Test move WEST
        /// </summary>
        [TestMethod]
        public void WhenRobotIsFacingEastThenMoveWillMakeOneStepForwardEast()
        {
            /* Set up data */
            var sensor = new Mock<ISurfaceSensor>();
            var coordinate = new Coordinate(1, 1);
            const Direction direction = Direction.EAST;

            sensor.Setup(s => s.IsOccupiable(It.IsAny<Coordinate>())).Returns(true);

            var robot = new ToyRobot(sensor.Object);
            robot.Place(coordinate, direction);

            /* Test */
            robot.Move();

            /* Check result */
            Assert.AreEqual(coordinate.X + 1, robot.CurrentLocation.X);
            Assert.AreEqual(coordinate.Y, robot.CurrentLocation.Y);
            Assert.AreEqual(direction, robot.CurrentDirection);
        }

        /// <summary>
        /// Test invalid move
        /// </summary>
        [TestMethod]
        public void WhenRobotIsOnTheEdgeThenMoveWillBeIgnore()
        {
            /* Set up data */
            var sensor = new Mock<ISurfaceSensor>();
            var coordinate = new Coordinate(1, 1);
            const Direction direction = Direction.EAST;

            sensor.Setup(s => s.IsOccupiable(coordinate)).Returns(true);

            var robot = new ToyRobot(sensor.Object);
            robot.Place(coordinate, direction);

            /* Test */
            robot.Move();

            /* Check result */
            Assert.AreEqual(coordinate.X, robot.CurrentLocation.X);
            Assert.AreEqual(coordinate.Y, robot.CurrentLocation.Y);
            Assert.AreEqual(direction, robot.CurrentDirection);
        }

        /// <summary>
        /// Test invalid move
        /// </summary>
        [TestMethod]
        public void WhenRobotIsNotReadyThenMoveWillBeIgnore()
        {
            /* Set up data */
            var sensor = new Mock<ISurfaceSensor>();

            sensor.Setup(s => s.IsOccupiable(It.IsAny<Coordinate>())).Returns(true);

            var robot = new ToyRobot(sensor.Object);

            /* Test */
            robot.Move();

            /* Check result */
            Assert.IsNull(robot.CurrentLocation);
        }

        #endregion

        #region Test turn left

        /// <summary>
        /// Test turn left from NORTH
        /// </summary>
        [TestMethod]
        public void WhenRobotIsFacingNorthThenTurnLeftWillMakeItFaceWestWithoutChangingLocation()
        {
            /* Set up data */
            var sensor = new Mock<ISurfaceSensor>();
            var coordinate = new Coordinate(1, 1);
            const Direction direction = Direction.NORTH;

            sensor.Setup(s => s.IsOccupiable(It.IsAny<Coordinate>())).Returns(true);

            var robot = new ToyRobot(sensor.Object);
            robot.Place(coordinate, direction);

            /* Test */
            robot.TurnLeft();

            /* Check result */
            Assert.AreEqual(coordinate.X, robot.CurrentLocation.X);
            Assert.AreEqual(coordinate.Y, robot.CurrentLocation.Y);
            Assert.AreEqual(Direction.WEST, robot.CurrentDirection);
        }

        /// <summary>
        /// Test turn left from SOUTH
        /// </summary>
        [TestMethod]
        public void WhenRobotIsFacingSouthThenTurnLeftWillMakeItFaceEastWithoutChangingLocation()
        {
            /* Set up data */
            var sensor = new Mock<ISurfaceSensor>();
            var coordinate = new Coordinate(1, 1);
            const Direction direction = Direction.SOUTH;

            sensor.Setup(s => s.IsOccupiable(It.IsAny<Coordinate>())).Returns(true);

            var robot = new ToyRobot(sensor.Object);
            robot.Place(coordinate, direction);

            /* Test */
            robot.TurnLeft();

            /* Check result */
            Assert.AreEqual(coordinate.X, robot.CurrentLocation.X);
            Assert.AreEqual(coordinate.Y, robot.CurrentLocation.Y);
            Assert.AreEqual(Direction.EAST, robot.CurrentDirection);
        }

        /// <summary>
        /// Test turn left from West
        /// </summary>
        [TestMethod]
        public void WhenRobotIsFacingWestThenTurnLeftWillMakeItFaceSouthWithoutChangingLocation()
        {
            /* Set up data */
            var sensor = new Mock<ISurfaceSensor>();
            var coordinate = new Coordinate(1, 1);
            const Direction direction = Direction.WEST;

            sensor.Setup(s => s.IsOccupiable(It.IsAny<Coordinate>())).Returns(true);

            var robot = new ToyRobot(sensor.Object);
            robot.Place(coordinate, direction);

            /* Test */
            robot.TurnLeft();

            /* Check result */
            Assert.AreEqual(coordinate.X, robot.CurrentLocation.X);
            Assert.AreEqual(coordinate.Y, robot.CurrentLocation.Y);
            Assert.AreEqual(Direction.SOUTH, robot.CurrentDirection);
        }

        /// <summary>
        /// Test turn left from East
        /// </summary>
        [TestMethod]
        public void WhenRobotIsFacingEastThenTurnLeftWillMakeItFaceNorthWithoutChangingLocation()
        {
            /* Set up data */
            var sensor = new Mock<ISurfaceSensor>();
            var coordinate = new Coordinate(1, 1);
            const Direction direction = Direction.EAST;

            sensor.Setup(s => s.IsOccupiable(It.IsAny<Coordinate>())).Returns(true);

            var robot = new ToyRobot(sensor.Object);
            robot.Place(coordinate, direction);

            /* Test */
            robot.TurnLeft();

            /* Check result */
            Assert.AreEqual(coordinate.X, robot.CurrentLocation.X);
            Assert.AreEqual(coordinate.Y, robot.CurrentLocation.Y);
            Assert.AreEqual(Direction.NORTH, robot.CurrentDirection);
        }

        /// <summary>
        /// Test invalid turn
        /// </summary>
        [TestMethod]
        public void WhenRobotIsNotReadyThenTurnLeftWillBeIgnore()
        {
            /* Set up data */
            var sensor = new Mock<ISurfaceSensor>();
            var robot = new ToyRobot(sensor.Object);
            var currentDirection = robot.CurrentDirection;

            /* Test */
            robot.TurnLeft();

            /* Check result */
            Assert.IsNull(robot.CurrentLocation);
            Assert.AreEqual(currentDirection, robot.CurrentDirection);
        }

        #endregion

        #region Test turn right

        /// <summary>
        /// Test turn right from NORTH
        /// </summary>
        [TestMethod]
        public void WhenRobotIsFacingNorthThenTurnRightWillMakeItFaceEastWithoutChangingLocation()
        {
            /* Set up data */
            var sensor = new Mock<ISurfaceSensor>();
            var coordinate = new Coordinate(1, 1);
            const Direction direction = Direction.NORTH;

            sensor.Setup(s => s.IsOccupiable(It.IsAny<Coordinate>())).Returns(true);

            var robot = new ToyRobot(sensor.Object);
            robot.Place(coordinate, direction);

            /* Test */
            robot.TurnRight();

            /* Check result */
            Assert.AreEqual(coordinate.X, robot.CurrentLocation.X);
            Assert.AreEqual(coordinate.Y, robot.CurrentLocation.Y);
            Assert.AreEqual(Direction.EAST, robot.CurrentDirection);
        }

        /// <summary>
        /// Test turn right from SOUTH
        /// </summary>
        [TestMethod]
        public void WhenRobotIsFacingSouthThenTurnRightWillMakeItFaceWestWithoutChangingLocation()
        {
            /* Set up data */
            var sensor = new Mock<ISurfaceSensor>();
            var coordinate = new Coordinate(1, 1);
            const Direction direction = Direction.SOUTH;

            sensor.Setup(s => s.IsOccupiable(It.IsAny<Coordinate>())).Returns(true);

            var robot = new ToyRobot(sensor.Object);
            robot.Place(coordinate, direction);

            /* Test */
            robot.TurnRight();

            /* Check result */
            Assert.AreEqual(coordinate.X, robot.CurrentLocation.X);
            Assert.AreEqual(coordinate.Y, robot.CurrentLocation.Y);
            Assert.AreEqual(Direction.WEST, robot.CurrentDirection);
        }

        /// <summary>
        /// Test turn right from WEST
        /// </summary>
        [TestMethod]
        public void WhenRobotIsFacingWestThenTurnRightWillMakeItFaceNorthWithoutChangingLocation()
        {
            /* Set up data */
            var sensor = new Mock<ISurfaceSensor>();
            var coordinate = new Coordinate(1, 1);
            const Direction direction = Direction.WEST;

            sensor.Setup(s => s.IsOccupiable(It.IsAny<Coordinate>())).Returns(true);

            var robot = new ToyRobot(sensor.Object);
            robot.Place(coordinate, direction);

            /* Test */
            robot.TurnRight();

            /* Check result */
            Assert.AreEqual(coordinate.X, robot.CurrentLocation.X);
            Assert.AreEqual(coordinate.Y, robot.CurrentLocation.Y);
            Assert.AreEqual(Direction.NORTH, robot.CurrentDirection);
        }

        /// <summary>
        /// Test turn right from EAST
        /// </summary>
        [TestMethod]
        public void WhenRobotIsFacingEastThenTurnRightWillMakeItFaceSouthWithoutChangingLocation()
        {
            /* Set up data */
            var sensor = new Mock<ISurfaceSensor>();
            var coordinate = new Coordinate(1, 1);
            const Direction direction = Direction.EAST;

            sensor.Setup(s => s.IsOccupiable(It.IsAny<Coordinate>())).Returns(true);

            var robot = new ToyRobot(sensor.Object);
            robot.Place(coordinate, direction);

            /* Test */
            robot.TurnRight();

            /* Check result */
            Assert.AreEqual(coordinate.X, robot.CurrentLocation.X);
            Assert.AreEqual(coordinate.Y, robot.CurrentLocation.Y);
            Assert.AreEqual(Direction.SOUTH, robot.CurrentDirection);
        }

        /// <summary>
        /// Test invalid turn right
        /// </summary>
        [TestMethod]
        public void WhenRobotIsNotReadyThenTurnRightWillBeIgnore()
        {
            /* Set up data */
            var sensor = new Mock<ISurfaceSensor>();
            var robot = new ToyRobot(sensor.Object);
            var currentDirection = robot.CurrentDirection;

            /* Test */
            robot.TurnRight();

            /* Check result */
            Assert.IsNull(robot.CurrentLocation);
            Assert.AreEqual(currentDirection, robot.CurrentDirection);
        }

        #endregion

        #region Test Reset

        /// <summary>
        /// Test place
        /// </summary>
        [TestMethod]
        public void WhenRobotIsReadyThenResetWillMakeItNotReadyAndClearCurrentLocation()
        {
            /* Set up data */
            var sensor = new Mock<ISurfaceSensor>();
            var coordinate = new Coordinate(1, 1);
            var direction = Direction.SOUTH;

            sensor.Setup(s => s.IsOccupiable(coordinate)).Returns(true);

            var robot = new ToyRobot(sensor.Object);
            robot.Place(coordinate, direction);

            /* Test */
            robot.Reset();

            /* Check result */
            Assert.IsFalse(robot.IsReady);
            Assert.IsNull(robot.CurrentLocation);
        }

        #endregion

        #region Complete moving test

        /// <summary>
        /// Test place
        /// </summary>
        [TestMethod]
        public void CompleteMovingTest()
        {

            var sensor = new SimpleSurfaceSensor(5, 5);
            var robot = new ToyRobot(sensor);
            //PLACE 2,0,NORTH
            PerformPlace(2, 0, Direction.NORTH, robot);
            //MOVE
            PerformMove(robot);
            //MOVE
            PerformMove(robot);
            //MOVE
            PerformMove(robot);
            //MOVE
            PerformMove(robot);
            //MOVE(Should be ignored)
            PerformMove(robot);
            //REPORT(Should output 2, 4, NORTH)
            CheckReport(2, 4, Direction.NORTH, robot);
            //LEFT
            PerformLeft(robot);
            //REPORT(Should output 2, 4, WEST)
            CheckReport(2, 4, Direction.WEST, robot);
            //MOVE
            PerformMove(robot);
            //MOVE
            PerformMove(robot);
            //MOVE(Should be ignored)
            PerformMove(robot);
            //REPORT(Should output 0, 4, WEST)
            CheckReport(0, 4, Direction.WEST, robot);
            //LEFT
            PerformLeft(robot);
            //REPORT(Should output 0, 4, SOUTH)
            CheckReport(0, 4, Direction.SOUTH, robot);
            //MOVE
            PerformMove(robot);
            //MOVE
            PerformMove(robot);
            //MOVE
            PerformMove(robot);
            //MOVE
            PerformMove(robot);
            //MOVE(Should be ignored)
            PerformMove(robot);
            //REPORT(Should output 0, 0, SOUTH)
            CheckReport(0, 0, Direction.SOUTH, robot);
            //LEFT
            PerformLeft(robot);
            //REPORT(Should output 0, 0, EAST)
            CheckReport(0, 0, Direction.EAST, robot);
            //MOVE
            PerformMove(robot);
            //MOVE
            PerformMove(robot);
            //MOVE
            PerformMove(robot);
            //MOVE
            PerformMove(robot);
            //MOVE(Should be ignored)
            PerformMove(robot);
            //REPORT(Should output 4, 0, EAST)
            CheckReport(4, 0, Direction.EAST, robot);
            //LEFT
            PerformLeft(robot);
            //REPORT(Should output 0, 0, NORTH)
            CheckReport(4, 0, Direction.NORTH, robot);
            //MOVE
            PerformMove(robot);
            //REPORT(Should output 4, 1, NORTH)
            CheckReport(4, 1, Direction.NORTH, robot);
            //RIGHT
            PerformRight(robot);
            //REPORT(Should output 4, 1, EAST)
            CheckReport(4, 1, Direction.EAST, robot);
            //RIGHT
            PerformRight(robot);
            //REPORT(Should output 4, 1, SOUTH)
            CheckReport(4, 1, Direction.SOUTH, robot);
            //RIGHT
            PerformRight(robot);
            //REPORT(Should output 4, 1, WEST)
            CheckReport(4, 1, Direction.WEST, robot);
            //PLACE 2,2,EAST
            PerformPlace(2, 2, Direction.EAST, robot);
            //REPORT(Should output 2, 2, EAST)
            CheckReport(2, 2, Direction.EAST, robot);
            //PLACE 1,1,WEST
            PerformPlace(1, 1, Direction.WEST, robot);
            //REPORT(Should output 1, 1, WEST)
            CheckReport(1, 1, Direction.WEST, robot);
            //PLACE 0,0,SOUTH
            PerformPlace(0, 0, Direction.SOUTH, robot);
            //REPORT(Should output 0, 0, SOUTH)
            PerformPlace(0, 0, Direction.SOUTH, robot);
            //LEFT
            PerformLeft(robot);
            //MOVE
            PerformMove(robot);
            //RIGHT
            PerformRight(robot);
            //REPORT(Should output 1, 0, SOUTH)
            CheckReport(1, 0, Direction.SOUTH, robot);            
        }

        private static void PerformMove(IRobot robot)
        {
            var command = new MoveCommand(robot);
            command.Execute();
        }

        private static void PerformLeft(IRobot robot)
        {
            var command = new TurnLeftCommand(robot);
            command.Execute();
        }

        private static void PerformRight(IRobot robot)
        {
            var command = new TurnRightCommand(robot);
            command.Execute();
        }

        private static void PerformPlace(int x, int y, Direction direction, IRobot robot)
        {
            var command = new PlaceCommand(new Coordinate(x, y), direction, robot);
            command.Execute();
        }

        private static void CheckReport(int x, int y, Direction direction, IRobot robot)
        {
            Assert.AreEqual(x, robot.CurrentLocation.X);
            Assert.AreEqual(y, robot.CurrentLocation.Y);
            Assert.AreEqual(direction, robot.CurrentDirection);
        }

        #endregion
    }
}
