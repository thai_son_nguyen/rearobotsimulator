﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using REARobotSimulator.Command;
using REARobotSimulator.Controller;
using REARobotSimulator.Navigation;
using REARobotSimulator.Robot;

namespace REARobotSimulatorTest.Controller
{
    [TestClass]
    public class ConsoleCommandParserTest
    {
        #region Test PLACE

        [TestMethod]
        public void WhenPlaceCommandHasInvalidKeyWordThenNullCommandWillBeCreated()
        {
            /* Set up data */
            var commandStr = "PLACES 1,1,NORTH";
            var robot = new Mock<IRobot>();

            /* Test */
            var command = ConsoleCommandParser.ParseCommand(commandStr, robot.Object);

            /* Check result */
            Assert.IsInstanceOfType(command, typeof(NullCommand));
        }

        [TestMethod]
        public void WhenPlaceCommandHasInvalidXCoordinateThenNullCommandWillBeCreated()
        {
            /* Set up data */
            var commandStr = "PLACE 1a,1,NORTH";
            var robot = new Mock<IRobot>();

            /* Test */
            var command = ConsoleCommandParser.ParseCommand(commandStr, robot.Object);

            /* Check result */
            Assert.IsInstanceOfType(command, typeof(NullCommand));
        }

        [TestMethod]
        public void WhenPlaceCommandHasInvalidYCoordinateThenNullCommandWillBeCreated()
        {
            /* Set up data */
            var commandStr = "PLACE 1,1a,NORTH";
            var robot = new Mock<IRobot>();

            /* Test */
            var command = ConsoleCommandParser.ParseCommand(commandStr, robot.Object);

            /* Check result */
            Assert.IsInstanceOfType(command, typeof(NullCommand));
        }

        [TestMethod]
        public void WhenPlaceCommandHasInvalidDirectionThenNullCommandWillBeCreated()
        {
            /* Set up data */
            var commandStr = "PLACE 1,1,NORTHS";
            var robot = new Mock<IRobot>();

            /* Test */
            var command = ConsoleCommandParser.ParseCommand(commandStr, robot.Object);

            /* Check result */
            Assert.IsInstanceOfType(command, typeof(NullCommand));
        }

        [TestMethod]
        public void WhenPlaceCommandIsValidThenPlaceCommandWillBeCreated()
        {
            /* Set up data */
            var commandStr = "PLACE 1,1,SOUTH";
            var robot = new Mock<IRobot>();

            /* Test */
            var command = ConsoleCommandParser.ParseCommand(commandStr, robot.Object);
            command.Execute();

            /* Check result */
            Assert.IsInstanceOfType(command, typeof(PlaceCommand));
            robot.Verify(r => r.Place(It.Is<Coordinate>(c => c.X == 1 && c.Y ==1), Direction.SOUTH), Times.Once);
        }

        #endregion

        #region Test MOVE

        [TestMethod]
        public void WhenMoveCommandIsInvalidThenNullCommandWillBeCreated()
        {
            /* Set up data */
            var commandStr = "MOVES";
            var robot = new Mock<IRobot>();

            /* Test */
            var command = ConsoleCommandParser.ParseCommand(commandStr, robot.Object);

            /* Check result */
            Assert.IsInstanceOfType(command, typeof(NullCommand));
        }

        [TestMethod]
        public void WhenMoveCommandIsValidThenMoveCommandWillBeCreated()
        {
            /* Set up data */
            var commandStr = "MOVE";
            var robot = new Mock<IRobot>();

            /* Test */
            var command = ConsoleCommandParser.ParseCommand(commandStr, robot.Object);
            command.Execute();

            /* Check result */
            Assert.IsInstanceOfType(command, typeof(MoveCommand));
            robot.Verify(r => r.Move(), Times.Once);
        }

        #endregion

        #region Test TURN LEFT

        [TestMethod]
        public void WhenTurnLeftCommandIsInvalidThenTurnLeftCommandWillBeCreated()
        {
            /* Set up data */
            var commandStr = "Left2";
            var robot = new Mock<IRobot>();

            /* Test */
            var command = ConsoleCommandParser.ParseCommand(commandStr, robot.Object);

            /* Check result */
            Assert.IsInstanceOfType(command, typeof(NullCommand));
        }

        [TestMethod]
        public void WhenTurnLeftCommandIsValidThenTurnLeftCommandWillBeCreated()
        {
            /* Set up data */
            var commandStr = "Left";
            var robot = new Mock<IRobot>();

            /* Test */
            var command = ConsoleCommandParser.ParseCommand(commandStr, robot.Object);
            command.Execute();

            /* Check result */
            Assert.IsInstanceOfType(command, typeof(TurnLeftCommand));
            robot.Verify(r => r.TurnLeft(), Times.Once);
        }

        #endregion

        #region Test TURN RIGHT

        [TestMethod]
        public void WhenTurnRightCommandIsInvalidThenTurnLeftCommandWillBeCreated()
        {
            /* Set up data */
            var commandStr = "Right2";
            var robot = new Mock<IRobot>();

            /* Test */
            var command = ConsoleCommandParser.ParseCommand(commandStr, robot.Object);

            /* Check result */
            Assert.IsInstanceOfType(command, typeof(NullCommand));
        }

        [TestMethod]
        public void WhenTurnRightCommandIsValidThenTurnRightCommandWillBeCreated()
        {
            /* Set up data */
            var commandStr = "Right";
            var robot = new Mock<IRobot>();

            /* Test */
            var command = ConsoleCommandParser.ParseCommand(commandStr, robot.Object);
            command.Execute();

            /* Check result */
            Assert.IsInstanceOfType(command, typeof(TurnRightCommand));
            robot.Verify(r => r.TurnRight(), Times.Once);
        }

        #endregion

        #region Test REPORT

        [TestMethod]
        public void WhenReportCommandIsInvalidThenReportCommandWillBeCreated()
        {
            /* Set up data */
            var commandStr = "RePORT2";
            var robot = new Mock<IRobot>();

            /* Test */
            var command = ConsoleCommandParser.ParseCommand(commandStr, robot.Object);

            /* Check result */
            Assert.IsInstanceOfType(command, typeof(NullCommand));
        }

        [TestMethod]
        public void WhenReportCommandIsValidThenReportCommandWillBeCreated()
        {
            /* Set up data */
            var commandStr = "RePORT";
            var robot = new Mock<IRobot>();

            /* Test */
            var command = ConsoleCommandParser.ParseCommand(commandStr, robot.Object);
            command.Execute();

            /* Check result */
            Assert.IsInstanceOfType(command, typeof(ReportCommand));
            robot.Verify(r => r.IsReady, Times.Once);            
        }

        #endregion
    }
}

