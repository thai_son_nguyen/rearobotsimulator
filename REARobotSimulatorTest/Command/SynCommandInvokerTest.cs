﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using REARobotSimulator.Command;

namespace REARobotSimulatorTest.Command
{
    [TestClass]
    public class SynCommandInvokerTest
    {
        /// <summary>
        /// Test invoke command
        /// </summary>
        [TestMethod]
        public void WhenACommandIsSetToBeInvokedThenInvokerWillCallExecuteMethodOfTheCommand()
        {
            /* Set up data */
            var command = new Mock<ICommand>();
            var invoker = new SynCommandInvoker();

            /* Test */
            invoker.Invoke(command.Object);

            /* Check result */
            command.Verify(r => r.Execute(), Times.Once);
        }
    }
}
