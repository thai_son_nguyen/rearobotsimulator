﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using REARobotSimulator.Command;
using REARobotSimulator.Navigation;
using REARobotSimulator.Robot;

namespace REARobotSimulatorTest.Command
{
    [TestClass]
    public class TurnLeftCommandTest
    {
        /// <summary>
        /// Test move
        /// </summary>
        [TestMethod]
        public void WhenTurnLeftCommandIsExecutedThenRobotWillTurnLeft()
        {
            /* Set up data */
            var robot = new Mock<IRobot>();
            var command = new TurnLeftCommand(robot.Object);

            /* Test */
            command.Execute();

            /* Check result */
            robot.Verify(r => r.Move(), Times.Never);
            robot.Verify(r => r.CurrentDirection, Times.Never);
            robot.Verify(r => r.CurrentLocation, Times.Never);
            robot.Verify(r => r.IsReady, Times.Never);
            robot.Verify(r => r.TurnLeft(), Times.Once);
            robot.Verify(r => r.TurnRight(), Times.Never);
            robot.Verify(r => r.Place(It.IsAny<Coordinate>(), It.IsAny<Direction>()), Times.Never);
        }
    }
}
