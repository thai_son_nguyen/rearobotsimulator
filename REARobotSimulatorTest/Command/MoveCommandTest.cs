﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using REARobotSimulator.Command;
using REARobotSimulator.Navigation;
using REARobotSimulator.Robot;

namespace REARobotSimulatorTest.Command
{
    [TestClass]
    public class MoveCommandTest
    {
        /// <summary>
        /// Test move
        /// </summary>
        [TestMethod]
        public void WhenMoveCommandIsExecutedThenRobotWillMove()
        {
            /* Set up data */
            var robot = new Mock<IRobot>();
            var command = new MoveCommand(robot.Object);

            /* Test */
            command.Execute();

            /* Check result */
            robot.Verify(r => r.Move(), Times.Once);
            robot.Verify(r => r.CurrentDirection, Times.Never);
            robot.Verify(r => r.CurrentLocation, Times.Never);
            robot.Verify(r => r.IsReady, Times.Never);
            robot.Verify(r => r.TurnLeft(), Times.Never);
            robot.Verify(r => r.TurnRight(), Times.Never);
            robot.Verify(r => r.Place(It.IsAny<Coordinate>(), It.IsAny<Direction>()), Times.Never);
        }
    }
}
