﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using REARobotSimulator.Command;
using REARobotSimulator.Navigation;
using REARobotSimulator.Robot;

namespace REARobotSimulatorTest.Command
{
    [TestClass]
    public class PlaceCommandTest
    {
        /// <summary>
        /// Test move
        /// </summary>
        [TestMethod]
        public void WhenPlaceCommandIsExecutedThenRobotWillBePlaced()
        {
            /* Set up data */
            var robot = new Mock<IRobot>();
            var coordinate = new Coordinate(1, 1);
            const Direction direction = Direction.EAST;
            var command = new PlaceCommand(coordinate, direction, robot.Object);

            /* Test */
            command.Execute();

            /* Check result */
            robot.Verify(r => r.Move(), Times.Never);
            robot.Verify(r => r.CurrentDirection, Times.Never);
            robot.Verify(r => r.CurrentLocation, Times.Never);
            robot.Verify(r => r.IsReady, Times.Never);
            robot.Verify(r => r.TurnLeft(), Times.Never);
            robot.Verify(r => r.TurnRight(), Times.Never);
            robot.Verify(r => r.Place(coordinate, direction), Times.Once);
        }
    }
}
