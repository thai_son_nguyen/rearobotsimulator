﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using REARobotSimulator.Command;
using REARobotSimulator.Navigation;
using REARobotSimulator.Robot;

namespace REARobotSimulatorTest.Command
{
    [TestClass]
    public class ReportCommandTest
    {
        /// <summary>
        /// Test move
        /// </summary>
        [TestMethod]
        public void WhenReportCommandIsExecutedThenRobotDetailsWillBeAccessed()
        {
            /* Set up data */
            var robot = new Mock<IRobot>();
            var command = new ReportCommand(robot.Object);
            robot.SetupGet(r => r.CurrentLocation).Returns(new Coordinate(1, 1));
            robot.SetupGet(r => r.CurrentDirection).Returns(Direction.EAST);
            robot.SetupGet(r => r.IsReady).Returns(true);

            /* Test */
            command.Execute();

            /* Check result */
            robot.Verify(r => r.Move(), Times.Never);
            robot.Verify(r => r.CurrentDirection, Times.AtLeastOnce);
            robot.Verify(r => r.CurrentLocation, Times.AtLeastOnce);
            robot.Verify(r => r.IsReady, Times.Once);
            robot.Verify(r => r.TurnLeft(), Times.Never);
            robot.Verify(r => r.TurnRight(), Times.Never);
            robot.Verify(r => r.Place(It.IsAny<Coordinate>(), It.IsAny<Direction>()), Times.Never);
        }

        /// <summary>
        /// Test move
        /// </summary>
        [TestMethod]
        public void WhenReportCommandIsExecutedAndRobotIsNotReadyThenRobotDetailsWillNotBeAccessed()
        {
            /* Set up data */
            var robot = new Mock<IRobot>();
            var command = new ReportCommand(robot.Object);
            robot.SetupGet(r => r.CurrentLocation).Returns(new Coordinate(1, 1));
            robot.SetupGet(r => r.CurrentDirection).Returns(Direction.EAST);
            robot.SetupGet(r => r.IsReady).Returns(false);

            /* Test */
            command.Execute();

            /* Check result */
            robot.Verify(r => r.Move(), Times.Never);
            robot.Verify(r => r.CurrentDirection, Times.Never);
            robot.Verify(r => r.CurrentLocation, Times.Never);
            robot.Verify(r => r.IsReady, Times.Once);
            robot.Verify(r => r.TurnLeft(), Times.Never);
            robot.Verify(r => r.TurnRight(), Times.Never);
            robot.Verify(r => r.Place(It.IsAny<Coordinate>(), It.IsAny<Direction>()), Times.Never);
        }
    }
}
